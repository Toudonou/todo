import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/constants.dart';
import 'package:todoapp/data/database.dart';
import 'package:todoapp/data/todo.dart';

class CreationPage extends StatefulWidget {
  @override
  State<CreationPage> createState() => _CreationPageState();

  const CreationPage({super.key});
}

class _CreationPageState extends State<CreationPage> {
  int selectedPriority = Priority.low;
  int selectedCategorie = 0;

  late TextEditingController titleController = TextEditingController();
  late TextEditingController timeController = TextEditingController();

  @override
  void initState() {
    titleController = TextEditingController();
    timeController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    titleController = TextEditingController();
    timeController = TextEditingController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<Database>(
      builder: (context, values, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              "Create New Task",
              style: TextStyle(
                color: Constant.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            centerTitle: true,
            leading: Constant.myButton(
              FontAwesomeIcons.xmark,
              Constant.black,
              Constant.lightWhite,
              false,
              () {
                Navigator.pop(context);
              },
            ),
            backgroundColor: Constant.lightWhite,
          ),
          backgroundColor: Constant.lightWhite,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(Constant.defaultPadding * 2),
                child: TextField(
                  key: const ValueKey(0),
                  controller: titleController,
                  cursorColor: Constant.black,
                  onTapOutside: (event) {
                    FocusManager.instance.primaryFocus?.unfocus();
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    labelText: 'Title',
                    floatingLabelStyle: TextStyle(
                      color: Constant.black,
                    ),
                  ),
                ),
              ),
              SizedBox(height: Constant.defaultPadding * 2),
              Container(
                margin: EdgeInsets.all(Constant.defaultPadding * 2),
                child: TextField(
                  key: const ValueKey(1),
                  controller: timeController,
                  cursorColor: Constant.black,
                  keyboardType: TextInputType.number,
                  onTapOutside: (event) {
                    FocusManager.instance.primaryFocus?.unfocus();
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    labelText: 'Time Estimated (min)',
                    floatingLabelStyle: TextStyle(
                      color: Constant.black,
                    ),
                  ),
                ),
              ),
              SizedBox(height: Constant.defaultPadding * 2),
              Container(
                margin: EdgeInsets.only(
                  left: Constant.defaultPadding * 2,
                  bottom: Constant.defaultPadding,
                ),
                child: Text(
                  "Priority",
                  style: TextStyle(
                    color: Constant.black,
                    fontSize: 19,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  priorityCard(context, "Low", Priority.low),
                  priorityCard(context, "Medium", Priority.medium),
                  priorityCard(context, "High", Priority.high),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                  top: Constant.defaultPadding * 2,
                  left: Constant.defaultPadding * 2,
                  bottom: Constant.defaultPadding,
                ),
                child: Text(
                  "Categorie",
                  style: TextStyle(
                    color: Constant.black,
                    fontSize: 19,
                  ),
                ),
              ),
              Wrap(
                children: List.generate(Categorie.categories.length, (index) {
                  return Container(
                    decoration: BoxDecoration(
                      color:
                          Constant.colorList[Categorie.categories[index].color],
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: true ? [Constant.myBoxshadow] : [],
                    ),
                    margin: EdgeInsets.symmetric(
                      horizontal: Constant.defaultPadding * 2,
                      vertical: Constant.defaultPadding,
                    ),
                    padding: EdgeInsets.only(
                      left: Constant.defaultPadding,
                      right: Constant.defaultPadding,
                    ),
                    height: 40,
                    //width: 145,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedCategorie = index;
                        });
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Constant.iconList[Categorie.categories[index].icon],
                            color: selectedCategorie != index
                                ? Constant.black
                                : Constant.white,
                          ),
                          const SizedBox(width: 10),
                          Text(
                            Categorie.categories[index].name,
                            style: TextStyle(
                              fontSize: 15,
                              color: selectedCategorie != index
                                  ? Constant.black
                                  : Constant.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
              Expanded(child: Container()),
              Container(
                margin: EdgeInsets.all(Constant.defaultPadding * 2),
                width: double.infinity,
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Todo todo = Todo(
                        title: titleController.value.text,
                        categorie: Categorie.categories[selectedCategorie],
                        priority: selectedPriority,
                        timeEstimated:
                            int.tryParse(timeController.value.text) ?? 0,
                        isDone: false,
                        isDelete: false,
                      );

                      context.read<Database>().addTodo(todo);

                      Navigator.pop(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 15),
                          width: 10,
                          height: 10,
                          color: Colors.transparent,
                          child: FaIcon(
                            FontAwesomeIcons.plus,
                            color: Constant.white,
                          ),
                        ),
                        const SizedBox(width: 20),
                        Text(
                          "Create Task",
                          style: TextStyle(
                            color: Constant.white,
                            fontSize: 19,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Container priorityCard(BuildContext context, String name, int priorityLevel) {
    return Container(
      margin: EdgeInsets.only(left: Constant.defaultPadding * 2),
      width: MediaQuery.of(context).size.width * 0.25,
      height: 50,
      decoration: BoxDecoration(
        color: selectedPriority != priorityLevel
            ? Constant.lightWhite
            : Colors.blue,
        borderRadius: BorderRadius.circular(20),
        boxShadow:
            selectedPriority != priorityLevel ? [Constant.myBoxshadow] : [],
      ),
      child: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              selectedPriority = priorityLevel;
            });
          },
          child: Text(
            name,
            style: TextStyle(
              color: selectedPriority != priorityLevel
                  ? Constant.black
                  : Constant.white,
            ),
          ),
        ),
      ),
    );
  }

  Container myTextField(
      int key, String label, TextEditingController controller) {
    return Container(
      margin: EdgeInsets.all(Constant.defaultPadding * 2),
      child: TextField(
        key: ValueKey(key),
        controller: controller,
        cursorColor: Constant.black,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          labelText: label,
          floatingLabelStyle: TextStyle(
            color: Constant.black,
          ),
        ),
      ),
    );
  }
}
