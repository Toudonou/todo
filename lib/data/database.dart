import 'package:flutter/foundation.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:todoapp/data/todo.dart';

class Database extends ChangeNotifier {
  final todoBox = Hive.box("todoDatabase");
  List<dynamic> todosStock = [];
  List<Todo> todos = [];
  String todoKey = "TODOLIST";

  Database() {
    fetchOldTodos();
  }

  void fetchOldTodos() {
    if (todoBox.get(todoKey) == null) {
      todos = [];
    } else {
      todosStock = todoBox.get(todoKey);
      for (int i = 0; i < todosStock.length; i++) {
        todos.add(todosStock[i] as Todo);
      }
    }
    notifyListeners();
  }

  void updateDatabase() {
    todoBox.put(todoKey, todos);
  }

  void addTodo(Todo todo) {
    todos.insert(0, todo);
    updateDatabase();
    notifyListeners();
  }

  void setTodoToDelete(int index) {
    todos[index].isDelete = !todos[index].isDelete;
    updateDatabase();
    notifyListeners();
  }

  void removeTodo(int index) {
    setTodoToDelete(index);
    Future.delayed(const Duration(seconds: 5), () {
      todos.remove(todos[index]);
      updateDatabase();
      notifyListeners();
    });
  }

  void alterTodo(int index) {
    todos[index].isDone = !todos[index].isDone;
    updateDatabase();
    notifyListeners();
  }
}
