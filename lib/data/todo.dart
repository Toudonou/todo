import 'package:hive/hive.dart';

part 'todo.g.dart';

@HiveType(typeId: 0)
class Todo extends HiveObject {
  @HiveField(0)
  String title;
  @HiveField(1)
  Categorie categorie;
  @HiveField(2)
  int priority;
  @HiveField(3)
  int timeEstimated;
  @HiveField(4)
  bool isDone;
  @HiveField(5)
  bool isDelete;

  Todo({
    required this.title,
    required this.categorie,
    required this.priority,
    required this.timeEstimated,
    required this.isDone,
    required this.isDelete,
  });

  factory Todo.fromJson(Map<String, dynamic> json) {
    return Todo(
      title: json['title'],
      categorie: Categorie.fromJson(json['categorie']),
      priority: json['priority'],
      timeEstimated: json['timeEstimated'],
      isDone: json['isDone'],
      isDelete: json['delete'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'categorie': categorie.toJson(),
      'priority': priority,
      'timeEstimated': timeEstimated,
      'isDone': isDone,
      'delete': isDelete,
    };
  }
}

class Priority {
  static int low = 0;
  static int medium = 1;
  static int high = 2;

  static List<int> priorities = [
    Priority.low,
    Priority.medium,
    Priority.high,
  ];
}

@HiveType(typeId: 1)
class Categorie extends HiveObject {
  @HiveField(0)
  final String name;
  @HiveField(1)
  final int icon;
  @HiveField(2)
  final int color;

  Categorie({required this.name, required this.icon, required this.color});

  static List<Categorie> categories = [
    Categorie(
      name: "Other",
      icon: 0,
      color: 0,
    ),
    Categorie(
      name: "Study",
      icon: 1,
      color: 1,
    ),
    Categorie(
      name: "Workout",
      icon: 2,
      color: 2,
    ),
    Categorie(
      name: "Work",
      icon: 3,
      color: 3,
    ),
    Categorie(
      name: "Money",
      icon: 4,
      color: 4,
    ),
    Categorie(
      name: "Programming",
      icon: 5,
      color: 5,
    ),
  ];

  factory Categorie.fromJson(Map<String, dynamic> json) {
    return Categorie(
      name: json['name'],
      icon: json['icon'] as int,
      color: json['color'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'icon': icon.toString(),
      'color': color.toString(),
    };
  }
}
