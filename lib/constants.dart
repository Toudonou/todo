import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todoapp/data/todo.dart';

class Constant {
  static Color lightWhite = const Color(0xFFF2EDED);
  static Color white = const Color(0xFFFFFFFF);
  static Color lightBlack = const Color(0xFF626262);
  static Color black = const Color(0xFF3A3A3A);
  static Color blue = const Color(0xFF1A73E9);

  static double defaultPadding = 10;
  static List<IconData> iconList = [
    FontAwesomeIcons.tag,
    FontAwesomeIcons.graduationCap,
    FontAwesomeIcons.dumbbell,
    FontAwesomeIcons.briefcase,
    FontAwesomeIcons.dollarSign,
    FontAwesomeIcons.code,
  ];

  static List<Color> colorList = [
    const Color(0xFFFFA500),
    const Color(0xFF3498DB),
    const Color(0xFF2ECC71),
    const Color(0xFF95A5A6),
    const Color(0xFF00FF00),
    const Color(0xFF800080),
  ];

  static List<String> days_of_week = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ];
  static List<String> months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  static BoxShadow myBoxshadow = BoxShadow(
    color: Constant.lightBlack.withOpacity(0.3),
    blurRadius: 10,
    spreadRadius: 0.0,
    offset: Offset.zero, // Shadow position
  );

  static Widget myButton(IconData icon, Color color, Color bgColor,
      bool haveShadow, Function()? func) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(100),
        boxShadow: haveShadow ? [Constant.myBoxshadow] : [],
      ),
      child: Center(
        child: GestureDetector(
          onTap: func,
          child: FaIcon(icon, color: color),
        ),
      ),
    );
  }

  static Widget categoriesIcon(
      Categorie categorie, bool isFinish, bool isDelete) {
    bool isNotFinishOrDelete = false;

    if (isFinish == true) isNotFinishOrDelete = true;
    if (isDelete == true) isNotFinishOrDelete = true;

    return Container(
      margin: EdgeInsets.all(Constant.defaultPadding),
      width: !isNotFinishOrDelete ? 50 : 30,
      height: !isNotFinishOrDelete ? 50 : 30,
      decoration: BoxDecoration(
        color:
            isNotFinishOrDelete ? Constant.white : colorList[categorie.color],
        borderRadius: BorderRadius.circular(100),
      ),
      child: Center(
        child: FaIcon(
          iconList[categorie.icon],
          size: (!isNotFinishOrDelete ? 50 : 30) / 2,
          color: !isNotFinishOrDelete
              ? Constant.white
              : isFinish
                  ? Colors.green
                  : Colors.red,
        ),
      ),
    );
  }
}
