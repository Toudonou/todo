import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/constants.dart';
import 'package:todoapp/data/todo.dart';
import 'package:todoapp/home_screen.dart';

import 'data/database.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      systemNavigationBarColor: Constant.lightWhite,
      systemNavigationBarDividerColor: Constant.lightWhite,
      systemNavigationBarContrastEnforced: true,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  await Hive.initFlutter();
  initHiveFunction();
  var box = await Hive.openBox('todoDatabase');

  runApp(
    ChangeNotifierProvider(
      create: (context) => Database(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          primary: Constant.lightWhite,
          seedColor: Constant.lightWhite,
        ),
        useMaterial3: true,
      ),
      home: const HomeScreen(),
    );
  }
}

void initHiveFunction() async {
  Directory directory = await getApplicationDocumentsDirectory();
  Hive.init(directory.path);
  Hive.registerAdapter(TodoAdapter());
  Hive.registerAdapter(CategorieAdapter());
}
