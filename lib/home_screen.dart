import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/constants.dart';
import 'package:todoapp/data/database.dart';
import 'package:todoapp/data/todo.dart';
import 'package:todoapp/todo_creation_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<Database>(
      builder: (context, values, child) {
        return Scaffold(
          appBar: AppBar(
            toolbarHeight: 0.0,
            backgroundColor: Constant.lightWhite,
          ),
          backgroundColor: Constant.lightWhite,
          body: Column(
            children: [
              header(context),
              Expanded(
                child: SingleChildScrollView(
                  child: values.todos.isNotEmpty
                      ? Column(
                          children: List.generate(
                            values.todos.length,
                            (index) {
                              return taskCard(index, values.todos);
                            },
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.4,
                          ),
                          child: Text(
                            "No tasks yet",
                            style: TextStyle(
                              color: Constant.black,
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Container taskCard(int index, List<Todo> todos) {
    bool isFinishOrDelete = false;

    if (todos[index].isDone == true) isFinishOrDelete = true;
    if (todos[index].isDelete == true) isFinishOrDelete = true;

    return Container(
      margin: EdgeInsets.only(
        top: Constant.defaultPadding,
        left: Constant.defaultPadding * 1.5,
        right: Constant.defaultPadding * 1.5,
      ),
      child: isFinishOrDelete
          ? taskCardContainer(index, todos)
          : Slidable(
              key: ValueKey(index),
              startActionPane: ActionPane(
                motion: const ScrollMotion(),
                children: [
                  SlidableAction(
                    backgroundColor: Colors.green,
                    foregroundColor: Colors.white,
                    icon: FontAwesomeIcons.check,
                    borderRadius: BorderRadius.circular(20),
                    onPressed: (BuildContext context) {
                      context.read<Database>().alterTodo(index);
                    },
                  ),
                ],
              ),
              endActionPane: ActionPane(
                motion: const ScrollMotion(),
                children: [
                  SlidableAction(
                    backgroundColor: Colors.red,
                    foregroundColor: Colors.white,
                    icon: FontAwesomeIcons.trash,
                    borderRadius: BorderRadius.circular(20),
                    onPressed: (BuildContext context) {
                      context.read<Database>().removeTodo(index);
                    },
                  ),
                ],
              ),
              child: taskCardContainer(index, todos),
            ),
    );
  }

  Container taskCardContainer(int index, List<Todo> todos) {
    bool isNotFinishOrDelete = false;
    String priorityStars = "";
    if (todos[index].isDone == true) isNotFinishOrDelete = true;
    if (todos[index].isDelete == true) isNotFinishOrDelete = true;

    for (int i = 0; i < todos[index].priority; i++) {
      priorityStars += '⭐';
    }

    return Container(
      width: double.infinity,
      height: !isNotFinishOrDelete ? 70 : 50,
      decoration: BoxDecoration(
        color: todos[index].isDone
            ? Colors.green
            : todos[index].isDelete
                ? Colors.red
                : Constant.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Constant.categoriesIcon(
            todos[index].categorie,
            todos[index].isDone,
            todos[index].isDelete,
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "${todos[index].title} $priorityStars",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: isNotFinishOrDelete ? Constant.white : Constant.black,
                  decoration: todos[index].isDelete
                      ? TextDecoration.lineThrough
                      : TextDecoration.none,
                  decorationStyle: TextDecorationStyle.solid,
                ),
              ),
              Text(
                "Estimated time : ${todos[index].timeEstimated} min",
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: isNotFinishOrDelete
                      ? Constant.lightWhite
                      : Constant.lightBlack,
                  decoration: todos[index].isDelete
                      ? TextDecoration.lineThrough
                      : TextDecoration.none,
                  decorationStyle: TextDecorationStyle.solid,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget header(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: Constant.defaultPadding,
        horizontal: Constant.defaultPadding * 1.5,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Constant.myButton(
            FontAwesomeIcons.terminal,
            Constant.black,
            Constant.white,
            false,
            () => null,
          ),
          //Constant.myButton(
          //    FontAwesomeIcons.chevronLeft,
          //  Constant.lightBlack,
          //    Colors.transparent,
          //    false,
          //    () => null,
          //  ),
          Column(
            children: [
              Text(
                "${Constant.months[DateTime.now().month - 1].substring(0, 3)} ${DateTime.now().day}${DateTime.now().day == 1 ? "st" : DateTime.now().day == 2 ? "nd" : DateTime.now().day == 3 ? "rd" : "th"}",
                style: TextStyle(color: Constant.black, fontSize: 19),
              ),
              Text(
                Constant.days_of_week[DateTime.now().weekday - 1],
                style: TextStyle(color: Constant.lightBlack, fontSize: 13),
              ),
            ],
          ),
          //Constant.myButton(
          //    FontAwesomeIcons.chevronRight,
          //    Constant.lightBlack,
          //    Colors.transparent,
          //    false,
          //    () => null,
          //  ),
          Constant.myButton(
            FontAwesomeIcons.plus,
            Constant.white,
            Constant.blue,
            true,
            () => Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (context) => const CreationPage(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
